import React, {useEffect, useState} from 'react';
import './App.css';
import {GrpcWebFetchTransport} from "@protobuf-ts/grpcweb-transport";
import {TaskSchedulerServiceClient} from "./protos/TaskSchedulerService.client";
import {Timestamp} from "./protos/google/protobuf/timestamp";
import {TaskItem} from "./protos/Data/TaskItem";

const grpcTransport = new GrpcWebFetchTransport({
  baseUrl: 'https://localhost:8275',
  format: 'binary'
});

const grpcClient = new TaskSchedulerServiceClient(grpcTransport);

function App() {
  const dateStr = (now: Date) => `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()}T${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
  
  const [abortController, setAbortController] = useState<AbortController | null>(null);
  
  const [newTaskClicked, setNewTaskClicked] = useState<boolean>(false);
  
  const [data, setData] = useState<string>('');
  const [start, setStart] = useState<string>(dateStr(new Date()))
  const [stop, setStop] = useState<string>(dateStr(new Date()))
  
  const [tasks, setTasks] = useState<{
    [id: string]: TaskItem
  }>({});
  
  const submit = async () => {
    const res = await grpcClient.add({
      id: '',
      data,
      start: Timestamp.fromDate(new Date(start)),
      stop: Timestamp.fromDate(new Date(stop))
    });
  
    console.log(res)
  
    setNewTaskClicked(false)
  }
  
  const call = () => {
    console.log('call')
    
    const controller = new AbortController();
    const abort = controller.signal;
    
    setAbortController(controller);
    
    const res = grpcClient.call({}, {
      abort
    })
    
    res.responses.onMessage(item => {
      switch (item.message.oneofKind) {
        case 'create':
          tasks[item.message.create.task!.id] = item.message.create.task!;
          break;
        case 'delete':
          delete tasks[item.message.delete.taskId];
          break;
      }
  
      console.log(tasks)
      
      setTasks({...tasks})
    });
  }
  
  useEffect(() => {
    call();
    return () => {
      abortController?.abort()
  
      console.log('aborted')
    }
  }, []);
  
  return (
    <div className="App">
      <header className="App-header">
        <div>
          {Object.keys(tasks).map(id =>
            <p key={id}>
              Data: {tasks[id].data},
              Start: {dateStr(Timestamp.toDate(tasks[id].start!))},
              Stop: {dateStr(Timestamp.toDate(tasks[id].stop!))}
            </p>
          )}
        </div>
        {newTaskClicked &&
          <div>
            <input 
              className="App-input" 
              placeholder="Data" 
              value={data}
              onInput={event => setData((event.target as HTMLInputElement).value)}
            />
            &nbsp;
            <input 
              className="App-input" 
              type="datetime-local" 
              step={1}
              value={start}
              onInput={event => setStart((event.target as HTMLInputElement).value)}
            />
            &nbsp;
            <input 
              className="App-input" 
              type="datetime-local" 
              step={1}
              value={stop}
              onInput={event => setStop((event.target as HTMLInputElement).value)}
            />
            &nbsp;
            <button className="App-button" onClick={() => submit()}>Add</button>
          </div>
        }
        {!newTaskClicked &&
          <a className="App-link" href="#" onClick={() => setNewTaskClicked(true)}>
            Add new task
          </a>  
        }
      </header>
    </div>
  );
}

export default App;
