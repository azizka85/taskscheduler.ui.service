﻿FROM node:18-alpine

ARG NODE_ENV=production
 
WORKDIR /app
COPY . .
WORKDIR "/app/taskscheduler.ui.service"
RUN npm i
RUN npm run proto:generate

ENTRYPOINT [ "sh", "start.sh" ]